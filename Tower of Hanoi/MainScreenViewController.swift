//
//  MainScreenViewController.swift
//  Tower of Hanoi
//
//  Created by Oleksandr Petrov on 9/21/18.
//  Copyright © 2018 Oleksandr Petrov. All rights reserved.
//

import UIKit

class MainScreenViewController: UIViewController {

    //MARK: Properties
    @IBOutlet weak private var towerAView: Tower!
    @IBOutlet weak private var towerBView: Tower!
    @IBOutlet weak private var towerCView: Tower!
    @IBOutlet weak private var stepsLabel: UILabel!
    @IBOutlet weak private var autoFinishButton: UIButton!
    
    private enum AutoFinishMode {
        case start, stop
    }
    private lazy var timer = Timer()
    private var timerIndex = 0
    var discQuantity: Int!
    private var stepsCounter = 0 {
        didSet {
            self.stepsLabel.text = "\(stepsCounter)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stepsLabel.text = "0"
        
        //MARK: Init the Tap Gesture Recognizers
        let towerATapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(towerAViewTapped))
        towerAView.addGestureRecognizer(towerATapGestureRecognizer)
        
        let towerBTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(towerBViewTapped))
        towerBView.addGestureRecognizer(towerBTapGestureRecognizer)
        
        let towerCTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(towerCViewTapped))
        towerCView.addGestureRecognizer(towerCTapGestureRecognizer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        towerAView.createAndLoadDiscs(quantity: discQuantity)
    }
    
    //MARK: Action methods of the Tap Gesture Recognizers
    @objc func towerAViewTapped() {
        towerAView.isSelected.toggle()
        moveDisc(toTower: towerAView, fromTower: towerBView, fromTower2: towerCView)
    }
    @objc func towerBViewTapped() {
        towerBView.isSelected.toggle()
        moveDisc(toTower: towerBView, fromTower: towerAView, fromTower2: towerCView)
    }
    @objc func towerCViewTapped() {
        towerCView.isSelected.toggle()
        moveDisc(toTower: towerCView, fromTower: towerAView, fromTower2: towerBView)
    }
    
    //MARK: Helpers methods
    private func cleanScene() {
        stepsCounter = 0
        timerIndex = 0
        [towerAView, towerBView, towerCView].forEach({
            $0.removeDiscs()
        })
        towerAView.createAndLoadDiscs(quantity: discQuantity)
    }
    
    private func checkCount() {
        stepsCounter += 1
        if towerCView.count == self.discQuantity {
            if autoFinishButton.isSelected {
                autoFinishButton.sendActions(for: .touchUpInside)
            }
            showAlert()
        }
    }
    
    private func showAlert() {
        let alert = UIAlertController(title: "You won,", message: "congratulations!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func towersUserInteractionEnabled(_ flag: Bool) {
        [towerAView, towerBView, towerCView].forEach({
            $0.isUserInteractionEnabled = flag
        })
    }
    
    private func moveDisc(toTower: Tower, fromTower: Tower, fromTower2: Tower) {
        func animateDeselection() {
            UIView.animate(withDuration: 0.5) {
                toTower.isSelected = false
                fromTower.isSelected = false
                fromTower2.isSelected = false
            }
        }
        func move(tower: Tower) {
            tower.moveDisc(to: toTower) { done in
                if done {
                    self.checkCount()
                }
            }
            animateDeselection()
        }
        if fromTower.isSelected {
            move(tower: fromTower)
        } else if fromTower2.isSelected {
            move(tower: fromTower2)
        }
    }
    
    //MARK: Start/stop the Auto Finish Mode
    private func autoFinish(_ mode: AutoFinishMode) {
        if mode == .start {
            towersUserInteractionEnabled(false)
            if towerAView.count != discQuantity || stepsCounter > 0 {
                cleanScene()
            }
            moveWithTimer()
        } else if mode == .stop {
            timer.invalidate()
            towersUserInteractionEnabled(true)
        }
    }
    
    private func moveWithTimer() {
        let moves: [(Tower, Tower)]!
        if discQuantity % 2 == 0 {
            moves = [(towerAView, towerBView), (towerAView, towerCView), (towerBView, towerCView)]
        } else {
            moves = [(towerAView, towerCView), (towerAView, towerBView), (towerBView, towerCView)]
        }
        timer = Timer.scheduledTimer(withTimeInterval: 0.75, repeats: true, block: { (timer) in
            if self.timerIndex == moves.count {
                self.timerIndex = 0
            }
            let a = moves[self.timerIndex].0, b = moves[self.timerIndex].1
            a.moveDisc(to: b, completion: { done in
                if done {
                    self.checkCount()
                } else {
                    b.moveDisc(to: a, completion: { done in
                        if done {
                            self.checkCount()
                        }
                    })
                }})
            self.timerIndex += 1
        })
    }
    
    // MARK: Actions
    @IBAction func resetButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func autoFinishButtonTapped(_ sender: UIButton) {
        autoFinishButton.isSelected.toggle()
        autoFinishButton.setTitle("Stop Auto Finish", for: .selected)
        if autoFinishButton.isSelected {
            autoFinish(.start)
        } else {
            autoFinish(.stop)
        }
    }
}
