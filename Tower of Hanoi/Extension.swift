//
//  Extension.swift
//  Tower of Hanoi
//
//  Created by Oleksandr Petrov on 10/8/18.
//  Copyright © 2018 Oleksandr Petrov. All rights reserved.
//

import UIKit

@IBDesignable extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}
