//
//  Tower.swift
//  Tower of Hanoi
//
//  Created by Oleksandr Petrov on 9/26/18.
//  Copyright © 2018 Oleksandr Petrov. All rights reserved.
//

import UIKit

final class Tower: UIView {
    
    //MARK: Properties
    var isSelected = false {
        didSet {
            changeTowerColor()
        }
    }
    var discs = [Disc]()
    var height: CGFloat {
        return self.frame.height
    }
    var width: CGFloat {
        return self.frame.width
    }
    var count: Int {
        return discs.count
    }
    
    //MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: Change the tower color
    func changeTowerColor() {
        if isSelected {
            self.backgroundColor = UIColor.darkGray
        } else {
            self.backgroundColor = UIColor(red: 244/255.0, green: 245/255.0, blue: 250/255.0, alpha: 1.0)
        }
    }
    
    //MARK: Create discs and load them into the tower
    func createAndLoadDiscs(quantity: Int) {
        var discWidth: CGFloat = self.width
        let discHeight: CGFloat = self.height / 25
        var discY: CGFloat = 0
        
        func randomColor() -> CGFloat {
            return CGFloat(drand48())
        }
        
        func discColor() -> UIColor {
            let red: CGFloat = randomColor()
            let green: CGFloat = randomColor()
            let blue: CGFloat = randomColor()
            return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        }
        
        for disc in 0..<quantity {
            discWidth -= self.width / (CGFloat(quantity) + 2)
            discY -= discHeight
            let discX: CGFloat = self.width / 2 - discWidth / 2
            let discView = Disc(frame: CGRect(x: discX, y: discY, width: discWidth, height: discHeight))
            discView.layer.cornerRadius = 5
            discView.backgroundColor = discColor()
            discView.tag = disc
            discs.append(discView)
            UIView.animate(withDuration: 0.25) {
                self.discs.last?.y += self.height
            }
            if let lastDisc = self.discs.last {
                self.addSubview(lastDisc)
            }
        }
    }
    
    //MARK: Move the top disc to another tower
    func moveDisc(to: Tower, completion: @escaping (Bool) -> Void) {
        if !self.discs.isEmpty {
            let toTag = to.discs.last?.tag ?? 0, selfTag = self.discs.last?.tag ?? 0
            if to.discs.isEmpty || toTag <  selfTag {
                if let selfDiscLast = self.discs.last {
                    to.discs.append(selfDiscLast)
                }
                UIView.animate(withDuration: 0.25, animations: {
                    self.discs.last?.y = 0
                }) { _ in
                    self.discs.removeLast()
                    to.loadLastDisc {
                        completion(true)
                    }
                }
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
    }
    
    func removeDiscs() {
        for disc in discs {
            disc.removeFromSuperview()
        }
        discs.removeAll()
    }
    
    //MARK: Load the last disc from array into the tower
    func loadLastDisc(completion: @escaping () -> Void) {
        if let lastDisc = discs.last {
            lastDisc.y = 0
            UIView.animate(withDuration: 0.25, animations: {
                lastDisc.y = self.height - lastDisc.height * CGFloat(self.count)
            }) { _ in
                completion()
            }
            self.addSubview(lastDisc)
        } else {
            completion()
        }
    }
}
