//
//  MenuViewController.swift
//  Tower of Hanoi
//
//  Created by Oleksandr Petrov on 9/21/18.
//  Copyright © 2018 Oleksandr Petrov. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    //MARK: Properties
    var numberOfDisks: Int!
    @IBOutlet weak var numberOfDisksTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberOfDisksTextField.delegate = self
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if let numberOfDisksText = numberOfDisksTextField.text, let number = Int(numberOfDisksText) {
            if number >= 3 && number <= 21 {
                numberOfDisks = number
                return true
            }
        }
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? MainScreenViewController {
            destinationVC.discQuantity = numberOfDisks
        }
    }
}

//MARK: TextField delegate
extension MenuViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        numberOfDisksTextField.resignFirstResponder()
        return true
    }
}
